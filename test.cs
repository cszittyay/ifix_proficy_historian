using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Proficy.Historian.UserAPI;

namespace Test
{
  class Program
  {
    static void CreateTags(int handle, string[] tags, ihuDataType type)
    {
      IHUAPI.ihuTagClearProperties();
      IHUAPI.ihuTagSetNumericProperty(ihuTagProperties.DataType, (double)type);
      IHUAPI.ihuTagSetNumericProperty(ihuTagProperties.HighEngineeringUnits, 10000.0);
      IHUAPI.ihuTagSetNumericProperty(ihuTagProperties.LowEngineeringUnits, 0.0);

      foreach (string tag in tags)
      {
        if (tag.Contains("XXXX")) continue;
        IHUAPI.ihuTagSetStringProperty(ihuTagProperties.Tagname, tag);
        IHUAPI.ihuTagSetStringProperty(ihuTagProperties.EnumSetName, "test");
        ihuErrorCode result = IHUAPI.ihuTagAdd(handle);
        if (result != ihuErrorCode.OK)
          Console.WriteLine("ihuTagAdd({0}) [{1}]", tag, result);
      }
    }
  
    static void TestPublishing(int handle)
    {              
        IHU_COLLECTOR[] collectors;
        string distributorName="";
        string originalDestinationServer = "", tempServer = "testserver", temp="";
        ihuErrorCode result = IHUAPI.ihuBrowseCollectors(handle, "*", out collectors);
        for (int i = 0; collectors != null && i < collectors.Length; i++)
        {
            if (collectors[i].CollectorType == ihuCollectorType.ServerToServerDistributor)
                distributorName = collectors[i].CollectorName;
        }
        if (distributorName == null)
            Console.WriteLine("No ServerToServer Distributor found on local server, existing publishing tests");
        else
        {
            Console.WriteLine(" Found ServerToServer Distributor - {0}", distributorName);
     
            result = IHUAPI.ihuPublishGetDestinationServer(handle, distributorName, ref originalDestinationServer);
            Console.WriteLine("Test get destination server={0} {1}", result, originalDestinationServer);
                       
            result = IHUAPI.ihuPublishSetDestinationServer(handle, distributorName, tempServer);
            Console.WriteLine("Test set destination server={0} {1}", result, tempServer);

            result = IHUAPI.ihuPublishGetDestinationServer(handle, distributorName, ref temp);
            Console.WriteLine("Test readback destination server={0} {1}", result, temp);

            result = IHUAPI.ihuPublishSetDestinationServer(handle, distributorName, originalDestinationServer);
            Console.WriteLine("Test restore original destination server={0} {1}", result, originalDestinationServer);

            result = IHUAPI.ihuPublishAddTag(handle, distributorName, "no such tag name", null);
            Console.WriteLine("Test add tag with unknown source tag={0}", result);            

            result = IHUAPI.ihuPublishAddTag(handle, distributorName, "Therese-Dev47.Simulation00002", "NewTag2");
            Console.WriteLine("Test add tag with known source tag={0}", result);            

            result = IHUAPI.ihuPublishTagSetStringProperty(ihuTagProperties.Description, "This is a tag description");
            result = IHUAPI.ihuPublishTagSetNumericProperty(ihuTagProperties.ArchiveCompression, 1.0);
            result = IHUAPI.ihuPublishSetTagProperties(handle, distributorName, "NewTag2");                    
            result = IHUAPI.ihuPublishTagClearProperties();
            
            int numberOfTagsFound;
            result = IHUAPI.ihuPublishGetTagPropertiesToCache(handle, distributorName, "*", out numberOfTagsFound);
            Console.WriteLine("Test initial tag query={0}", result);

            if (numberOfTagsFound == 0)
                Console.WriteLine("No tags found");
            else
            {
                Console.WriteLine("{0} Tags found", numberOfTagsFound);

                for (int tagIndex = 0; tagIndex < numberOfTagsFound; tagIndex++)
                {                   
                    for (int i = 0; i < (int)ihuTagProperties.MaxPropertyNum + 2; i++)
                    {
                        string strVal;
                        result = IHUAPI.ihuPublishTagGetStringPropertyByIndex(tagIndex, (ihuTagProperties)i, out strVal);
                        if (result == ihuErrorCode.OK)
                        {
                            Console.WriteLine("ihuPublishTagGetStringPropertyByIndex({0}, {1})=\"{2}\" [{3}]", tagIndex, (ihuTagProperties)i, strVal, result);
                        }
                        else
                        {
                            double doubleVal;
                            result = IHUAPI.ihuPublishTagGetNumericPropertyByIndex(tagIndex, (ihuTagProperties)i, out doubleVal);
                            Console.WriteLine("ihuPublishTagGetNumericPropertyByIndex({0}, {1})={2:0.00} [{3}]", tagIndex, (ihuTagProperties)i, doubleVal, result);
                        }
                    }
                }
                string strVal1;
                result = IHUAPI.ihuPublishTagGetStringPropertyByTagname("NewTag2", ihuTagProperties.Description, out strVal1);
                Console.WriteLine("ihuPublishTagGetStringPropertyByTagname({0})=\"{1}\" [{2}]", "NewTag2 - Description", strVal1, result);

                double doubleVal1;
                result = IHUAPI.ihuPublishTagGetNumericPropertyByTagname("NewTag2", ihuTagProperties.CollectionInterval, out doubleVal1);
                Console.WriteLine("ihuPublishTagGetNumericPropertyByTagname({0})=\"{1}\" [{2}]", "NewTag2 - CollectionInterval", doubleVal1, result);

                result = IHUAPI.ihuPublishTagCloseCache();              
            }
            result = IHUAPI.ihuPublishRemoveTag(handle, distributorName, "NewTag2");
            Console.WriteLine("Test remove tag ={0}", result);            
        }    
    }

    static void CreateEnumeratedSets(int handle)
    {        
        EnumeratedSet myEnumSet = new EnumeratedSet();
        myEnumSet.Name = "testSetNew588a";
        myEnumSet.AdministratorSecurityGroup = "AdminGroup";
        myEnumSet.Description = "Sample Enumerated Set";
        myEnumSet.StateRawValueDataType = ihuDataType.DoubleFloat;

        EnumeratedSetStateName myStateName = new EnumeratedSetStateName();
        myStateName.StateName = "testState";
        myStateName.ReservedForFuture = 99;

        EnumeratedSetState myState = new EnumeratedSetState();
        myState.StateLowRawValue = 1;
        myState.StateHighRawValue = 2;
        myState.Description = "Sample Enumerated State";
        myState.StatesNames.Add(myStateName);

        myEnumSet.EnumeratedState.Add(myState);
        ihuErrorCode result= IHUAPI.ihuEnumeratedSetAdd(handle, myEnumSet);

        if (result == ihuErrorCode.OK)
        {
            EnumeratedSetStateName myStateName2 = new EnumeratedSetStateName();
            myStateName2.StateName = "testState2";
            myStateName2.ReservedForFuture = 99;

            EnumeratedSetState myState2 = new EnumeratedSetState();
            myState2.StateLowRawValue = 4;
            myState2.StateHighRawValue = 5;
            myState2.Description = "state 2 testing";
            myState2.StatesNames.Add(myStateName2);

            result = IHUAPI.ihuEnumeratedStateAdd(handle, myEnumSet.Name, myState2);
        }

        if (result == ihuErrorCode.OK)
        {
            EnumeratedSetStateName myStateName2 = new EnumeratedSetStateName();
            myStateName2.StateName = "testState2";
            myStateName2.ReservedForFuture = 99;

            EnumeratedSetState myState2 = new EnumeratedSetState();
            myState2.StateLowRawValue = 10;
            myState2.StateHighRawValue = 15;
            myState2.Description = "state 2 testing - new";
            myState2.StatesNames.Add(myStateName2);

            result = IHUAPI.ihuEnumeratedStateModify(handle, myEnumSet.Name, myStateName.StateName, myState2);
        }
    }

    static void TestEnumeratedSets(int handle)
    {
        ihuErrorCode result = IHUAPI.ihuEnumeratedSetRename(handle, "testSetNew5", "testSetNew5New");
        result = IHUAPI.ihuEnumeratedSetDelete(handle, "testSetNew8");
        result = IHUAPI.ihuEnumeratedStateDelete(handle, "testSetNew7", "testState2");

        List<EnumeratedSet> enumlist;
        result = IHUAPI.ihuGetEnumeratedSets(handle, "*", out enumlist);

        Console.WriteLine("ihuGetEnumeratedSets {0}", result);

        foreach (EnumeratedSet set in enumlist)
        {           
            Console.WriteLine("{0} number of states={1}, description = {2}", set.Name, set.NumberOfStates, set.Description);
            foreach(EnumeratedSetState state in set.EnumeratedState)
            {
                Console.WriteLine("  {0} High: {1} Low={2}", state.StatesNames[0].StateName, state.StateHighRawValue.ToString(), state.StateLowRawValue.ToString());
            }
        }
    }

    static void ReadRawDataByTime(int handle, string[] tags)
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(DateTime.Now.AddDays(-1));
      IHU_TIMESTAMP end = new IHU_TIMESTAMP();

      Console.WriteLine();
      Console.WriteLine("ihuReadRawDataByTime");
      foreach (string tag in tags)
      {
        IHU_DATA_SAMPLE[] values;
        ihuErrorCode result = IHUAPI.ihuReadRawDataByTime(handle, tag, start, end, out values);
        Console.WriteLine("{0} values={1} [{2}]", tag, values == null ? 0 : values.Length, result);
        for (int i = 0; i < (values == null ? 0 : values.Length); i++)
        {
          using (IHU_DATA_SAMPLE sample = values[i])
            Console.WriteLine("  {0} {1} value={2:0}", sample.TimeStamp, sample.Quality, sample.ValueObject);
        }
      }
    }

    static void ReadRawDataByCount(int handle, string[] tags, int numSamples, bool forwardTimeOrder)
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(forwardTimeOrder ? DateTime.Now.AddDays(-1) : DateTime.Now);

      Console.WriteLine();
      Console.WriteLine("ihuReadRawDataByCount(numSamples={0}, forwardTimeOrder={1})", numSamples, forwardTimeOrder);
      foreach (string tag in tags)
      {
        IHU_DATA_SAMPLE[] values;
        ihuErrorCode result = IHUAPI.ihuReadRawDataByCount(handle, tag, start, numSamples, forwardTimeOrder, out values);
        Console.WriteLine("{0} values={1} [{2}]", tag, values == null ? 0 : values.Length, result);
        for (int i = 0; i < (values == null ? 0 : values.Length); i++)
        {
          using (IHU_DATA_SAMPLE sample = values[i])
            Console.WriteLine("  {0} {1} value={2:0}", sample.TimeStamp, sample.Quality, sample.ValueObject);
        }
      }
    }

    static void RetrieveSampledData(int handle, string[] tags)
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP();
      IHU_TIMESTAMP end = new IHU_TIMESTAMP(DateTime.Now);

      IHU_RETRIEVED_DATA_VALUES[] tagValues;
      ihuErrorCode[] errors;
      ihuErrorCode result = IHUAPI.ihuRetrieveSampledData(handle, tags, start, end, ihuSamplingMode.Interpolated, 24, 60 * 60 * 1000, out tagValues, out errors);

      Console.WriteLine();
      Console.WriteLine("ihuRetrieveSampledData={0}", result);
      for (int i = 0; i < tagValues.Length; i++)
      {
        using (IHU_RETRIEVED_DATA_VALUES value = tagValues[i])
        {
          Console.WriteLine("{0} [{1}] num={2}", value.Tagname, errors[i], value.NumberOfValues);

          IHU_TIMESTAMP[] times = value.TimeStamps;
          ihuValue[] values = value.DataValues;
          float[] percents = value.PercentGoods;
          IHU_COMMENT[][] comments = value.Comments;
          for (int j = 0; j < values.Length; j++)
          {
            Console.WriteLine("  {0} value={1:0} good={2}%", times[j], values[j].AsObject(value.ValueDataType), percents[j]);
            if (comments != null && comments[i] != null)
              foreach (IHU_COMMENT comment in comments[i])
                Console.WriteLine("    {0}", comment);
          }
        }
      }
    }

    static void ReadMultiTagRawDataByTime(int handle, string[] tags)
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(DateTime.Now.AddHours(-1));
      IHU_TIMESTAMP end = new IHU_TIMESTAMP();

      IHU_RETRIEVED_RAW_VALUES[] rawValues;
      ihuErrorCode[] errors;
      ihuErrorCode result = IHUAPI.ihuReadMultiTagRawDataByTime(handle, tags, start, end, out rawValues, out errors);

      Console.WriteLine();
      Console.WriteLine("ihuReadMultiTagRawDataByTime={0}", result);
      for (int i = 0; i < rawValues.Length; i++)
      {
        using (IHU_RETRIEVED_RAW_VALUES value = rawValues[i])
        {
          Console.WriteLine("{0} [{1}] num={2}", value.Tagname, errors[i], value.NumberOfValues);
          IHU_DATA_SAMPLE[] samples = value.Values;
          for (int j = 0; j < value.NumberOfValues; j++)
          {
            Console.WriteLine("  {0} {1} value={2:0}", samples[j].TimeStamp, samples[j].Quality, samples[j].ValueObject);
            IHU_COMMENT[] comments = samples[j].Comments;
            if (comments != null)
              foreach (IHU_COMMENT comment in comments)
                Console.WriteLine("    {0}", comment);
          }
        }
      }
    }

    static void ReadMultiTagRawDataByCount(int handle, string[] tags, int numSamples, bool forwardTimeOrder)
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(forwardTimeOrder ? DateTime.Now.AddHours(-1) : DateTime.Now);

      IHU_RETRIEVED_RAW_VALUES[] rawValues;
      ihuErrorCode[] errors;
      ihuErrorCode result = IHUAPI.ihuReadMultiTagRawDataByCount(handle, tags, start, numSamples, forwardTimeOrder, out rawValues, out errors);

      Console.WriteLine();
      Console.WriteLine("ihuReadMultiTagRawDataByCount={0}", result);
      for (int i = 0; i < rawValues.Length; i++)
      {
        using (IHU_RETRIEVED_RAW_VALUES value = rawValues[i])
        {
          Console.WriteLine("{0} [{1}] num={2}", value.Tagname, errors[i], value.NumberOfValues);
          IHU_DATA_SAMPLE[] samples = value.Values;
          for (int j = 0; j < value.NumberOfValues; j++)
          {
            Console.WriteLine("  {0} {1} value={2:0}", samples[j].TimeStamp, samples[j].Quality, samples[j].ValueObject);
            IHU_COMMENT[] comments = samples[j].Comments;
            if (comments != null)
              foreach (IHU_COMMENT comment in comments)
                Console.WriteLine("    {0}", comment);
          }
        }
      }
    }

    static void ReadInterpolatedValue(int handle, string[] tags)
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(DateTime.Now.AddHours(-1));

      IHU_TIMESTAMP[] timestamps = { start, start, start, start };
      IHU_DATA_SAMPLE[] dataSamples;
      ihuErrorCode[] errors;
      ihuErrorCode result = IHUAPI.ihuReadInterpolatedValue(handle, tags, timestamps, out dataSamples, out errors);

      Console.WriteLine();
      Console.WriteLine("ihuReadInterpolatedValue={0}", result);
      for (int i = 0; i < dataSamples.Length; i++)
      {
        using (IHU_DATA_SAMPLE sample = dataSamples[i])
        {
          if (errors[i] == ihuErrorCode.OK)
            Console.WriteLine("{0} value={1:0} [{2}]", sample.Tagname, sample.ValueObject, errors[i]);
          else
            Console.WriteLine("{0} [{1}]", sample.Tagname, errors[i]);
        }
      }
    }

    static void ReadCurrentValue(int handle, string[] tags)
    {
      IHU_DATA_SAMPLE[] dataSamples;
      ihuErrorCode[] errors;
      ihuErrorCode result = IHUAPI.ihuReadCurrentValue(handle, tags, out dataSamples, out errors);

      Console.WriteLine();
      Console.WriteLine("ihuReadCurrentValue={0}", result);
      for (int i = 0; i < dataSamples.Length; i++)
      {
        using (IHU_DATA_SAMPLE sample = dataSamples[i])
        {
          if (errors[i] == ihuErrorCode.OK)
            Console.WriteLine("{0} {1} {2} value={3:0} [{4}]", sample.Tagname, sample.TimeStamp, sample.Quality, sample.ValueObject, errors[i]);
          else
            Console.WriteLine("{0} [{1}]", sample.Tagname, errors[i]);
        }
      }
    }

    static void WriteData<T>(int handle, string[] tags, T[] values)
    {
      int count = tags.Length * values.Length;

      IHU_DATA_SAMPLE[] samples = new IHU_DATA_SAMPLE[count];
      ihuErrorCode[] results = new ihuErrorCode[count];

      IHU_TIMESTAMP time = new IHU_TIMESTAMP(DateTime.Now);
      time.Subseconds = 0;

      int idx = 0;
      for (int i = 0; i < tags.Length; i++)
      {
        for (int j = 0; j < values.Length; j++)
        {
          samples[idx] = new IHU_DATA_SAMPLE();
          samples[idx].Tagname = tags[i];
          samples[idx].TimeStamp = time;
          samples[idx].TimeStamp.Seconds -= values.Length - 1 - j;
          samples[idx].ValueObject = values[j];
          samples[idx].Quality.QualityStatus = ihuQualityStatus.OPCGood;
          idx++;
        }
      }

      ihuErrorCode result = IHUAPI.ihuWriteData(handle, samples, results, true, false);
      for (int i = 0; i < count; i++)
        samples[i].Dispose();

      Console.WriteLine();
      Console.WriteLine("ihuWriteData({0}) [{1}]", count, result);
      for (int i = 0; i < count; i++)
        if (results[i] != ihuErrorCode.OK && results[i] != ihuErrorCode.INVALID_TAGNAME)
          Console.WriteLine("{0}:{1}", i, results[i]);

      for (int i = 0; i < tags.Length; i++)
      {
        result = IHUAPI.ihuWriteComment(handle, tags[i], time, "some comment string", null, null);
        Console.WriteLine("ihuWriteComment({0}, {1}) [{2}]", tags[i], time, result);
      }
    }

    static void GetArchiverProperty(int handle, string dataStore, string[] props)
    {
      Console.WriteLine();
      ihuErrorCode result;
      foreach (string prop in props)
      {
        string value;       
        if(dataStore == null)
            result = IHUAPI.ihuGetArchiverProperty(handle, prop, out value);
        else
            result = IHUAPI.ihuGetArchiverPropertyEx(handle, dataStore, prop, out value);
        Console.WriteLine("ihuGetArchiverProperty({0})=\"{1}\" [{2}]", prop, value, result);
      }
    }

    static void SetArchiverProperty(int handle, string dataStore, string[] props, string[] values)
    {
      Console.WriteLine();
      ihuErrorCode result;
      for (int i = 0; i < props.Length; i++)
      {
        if(dataStore == null)
            result = IHUAPI.ihuSetArchiverProperty(handle, props[i], values[i]);
        else
            result = IHUAPI.ihuSetArchiverPropertyEx(handle, dataStore, props[i], values[i]);
        Console.WriteLine("ihuSetArchiverProperty({0}, {1}) [{2}]", props[i], values[i], result);
      }
    }

    static void GetTagPropertiesByTagname(string tagname)
    {
      Console.WriteLine();
      for (int i = 0; i < (int)ihuTagProperties.MaxPropertyNum + 2; i++)
      {
        string strVal;
        ihuErrorCode result = IHUAPI.ihuGetStringTagPropertyByTagname(tagname, (ihuTagProperties)i, out strVal);
        if (result == ihuErrorCode.OK)
        {
          Console.WriteLine("ihuGetStringTagPropertyByTagname({0}, {1})=\"{2}\" [{3}]", tagname, (ihuTagProperties)i, strVal, result);
        }
        else
        {
          double doubleVal;
          result = IHUAPI.ihuGetNumericTagPropertyByTagname(tagname, (ihuTagProperties)i, out doubleVal);
          Console.WriteLine("ihuGetNumericTagPropertyByTagname({0}, {1})={2:0.00} [{3}]", tagname, (ihuTagProperties)i, doubleVal, result);
        }
      }
    }

    static void GetTagPropertiesByIndex(int tagIndex)
    {
      Console.WriteLine();
      for (int i = 0; i < (int)ihuTagProperties.MaxPropertyNum + 2; i++)
      {
        string strVal;
        ihuErrorCode result = IHUAPI.ihuGetStringTagPropertyByIndex(tagIndex, (ihuTagProperties)i, out strVal);
        if (result == ihuErrorCode.OK)
        {
          Console.WriteLine("ihuGetStringTagPropertyByIndex({0}, {1})=\"{2}\" [{3}]", tagIndex, (ihuTagProperties)i, strVal, result);
        }
        else
        {
          double doubleVal;
          result = IHUAPI.ihuGetNumericTagPropertyByIndex(tagIndex, (ihuTagProperties)i, out doubleVal);
          Console.WriteLine("ihuGetNumericTagPropertyByIndex({0}, {1})={2:0.00} [{3}]", tagIndex, (ihuTagProperties)i, doubleVal, result);
        }
      }
    }


    static void TagAddRenameDelete(int handle)
    {
      long num = DateTime.Now.Ticks & 1000000;

      string name = "Tag" + num;

      IHUAPI.ihuTagClearProperties();
      IHUAPI.ihuTagSetStringProperty(ihuTagProperties.Tagname, name);
      IHUAPI.ihuTagSetNumericProperty(ihuTagProperties.DataType, (double)ihuDataType.Integer);

      Console.WriteLine();

      ihuErrorCode result = IHUAPI.ihuTagAdd(handle);
      Console.WriteLine("ihuTagAdd({0}) [{1}]", name, result);

      name = "Tag" + (num + 1);
      IHUAPI.ihuTagSetStringProperty(ihuTagProperties.Tagname, name);
      result = IHUAPI.ihuTagAdd(handle);
      Console.WriteLine("ihuTagAdd({0}) [{1}]", name, result);

      string newname = name + "_renamed";
      result = IHUAPI.ihuTagRename(handle, name, newname);
      Console.WriteLine("ihuTagRename({0}, {1}) [{2}]", name, newname, result);

      result = IHUAPI.ihuTagDelete(handle, name);
      Console.WriteLine("ihuTagDelete({0}) [{1}]", name, result);

      result = IHUAPI.ihuTagDelete(handle, newname);
      Console.WriteLine("ihuTagDelete({0}) [{1}]", newname, result);
    }

    static void Timestamps()
    {
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(DateTime.Now);

      DateTime time;
      ihuErrorCode result = IHUAPI.IHU_TIMESTAMP_ToParts(start, out time);
      Console.WriteLine();
      Console.WriteLine("IHU_TIMESTAMP_ToParts={0:M/dd/yyyy HH:mm:ss.fff} [{1}]", time.ToLocalTime(), result);

      IHUAPI.IHU_TIMESTAMP_FromParts(time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second, time.Millisecond * IHU_TIMESTAMP.SubsecondsPerMillisecond, out start);
      int year, month, day, hour, minute, second, subsecond;
      
      result = IHUAPI.IHU_TIMESTAMP_ToParts(start, out year, out month, out day, out hour, out minute, out second, out subsecond);
      time = new DateTime(year, month, day, hour, minute, second, subsecond / IHU_TIMESTAMP.SubsecondsPerMillisecond, DateTimeKind.Local);
      Console.WriteLine("IHU_TIMESTAMP_ToParts={0:M/dd/yyyy HH:mm:ss.fff} [{1}]", time.ToLocalTime(), result);
      Console.WriteLine("           ToDateTime={0:M/dd/yyyy HH:mm:ss.fff}", start.ToDateTime().ToLocalTime());
    }

    static void BrowseCollectors(int handle)
    {
      IHU_COLLECTOR[] collectors;
      ihuErrorCode result = IHUAPI.ihuBrowseCollectors(handle, "*", out collectors);
      Console.WriteLine();
      Console.WriteLine("ihuBrowseCollectors={0} [{1}]", collectors == null ? 0 : collectors.Length, result);
      for (int i = 0; collectors != null && i < collectors.Length; i++)
        Console.WriteLine("  {0}", collectors[i]);
    }

    static void BrowseQueryModifiers(int handle)
    {
        IHU_QUERY_MODIFIER[] queryModifiers;
        ihuErrorCode result = IHUAPI.ihuBrowseQueryModifiers(handle, out queryModifiers);
        Console.WriteLine();
        Console.WriteLine("ihuBrowseQueryModifiers={0} [{1}]", queryModifiers == null ? 0 : queryModifiers.Length, result);
        for (int i = 0; queryModifiers != null && i < queryModifiers.Length; i++)
            Console.WriteLine("  {0}", queryModifiers[i].QueryModifierName);
    }

    /// <summary>
    /// Sample code demonstrating how to invoke IHUAPI.dll from C# code.
    /// 
    /// Compile along with ihuAPI.cs via:
    ///   csc.exe test.cs ihuAPI.cs
    ///   
    /// ihuAPI.cs abstracts both 32bit and 64bit entry points but you have to match the appropriate IHUAPI.dll
    /// to the OS or suffer a BadImageFormatException.
    /// 
    /// You can force an assembly to run as a 32bit process on a 64bit OS via the corflags.exe utility:
    ///   corflags.exe [assembly] /32bit+
    ///   corflags.exe [assembly] /32bit-
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
      ihuErrorCode result = IHUAPI.ihuSetConnectionParameters(new IHU_CONNECTION_PARAMETERS(100));
      Console.WriteLine("ihuSetConnectionParameters() [{0}]", result);
      result = IHUAPI.ihuRestoreDefaultConnectionParameters();
      Console.WriteLine("ihuRestoreDefaultConnectionParameters() [{0}]", result);      

      int handle;
      result = IHUAPI.ihuConnect("localhost", "", "", out handle);
      Console.WriteLine();
      Console.WriteLine("Connect={0} handle={1}", result, handle);

      Console.WriteLine("ihuIsServerConnected({0})={1}", handle, IHUAPI.ihuIsServerConnected(handle));
      Console.WriteLine("ihuIsServerConnected({0})={1}", handle / 2, IHUAPI.ihuIsServerConnected(handle / 2));

      int major, minor, build, revision;
      result = IHUAPI.ihuGetServerVersion(handle, out major, out minor, out build, out revision);
      Console.WriteLine("ihuGetServerVersion() [{0}.{1}.{2}.{3}]", major, minor, build, revision);

      string[] sampleTags = { "test" };
      IHUAPI.ihuSetQueryModifiers(handle, "#ENUMNATIVEVALUE");
      ReadRawDataByCount(handle, sampleTags, 10, true);
      IHUAPI.ihuClearQueryModifiers();
             
      DateTime starttime = new DateTime(2012,11,28,10,0,0,DateTimeKind.Local);
      IHU_TIMESTAMP start = new IHU_TIMESTAMP(starttime);
      DateTime endtime = new DateTime(2012, 11, 28, 11, 0, 0, DateTimeKind.Local);
      IHU_TIMESTAMP end = new IHU_TIMESTAMP(endtime);
      ihuValue tempValue = new ihuValue();
      tempValue.StringValue = "ON";
      IHU_DATA_INTERVAL interval;
      interval.Interval=0;
      interval.IntervalType = ihuIntervalType.IntervalMinute;
      IHU_RETRIEVED_DATA_VALUES_EX [] DataRecords;
      ihuErrorCode[] errors;
      result = IHUAPI.ihuRetrieveCalculatedDataEx2(handle, sampleTags, start, end, ihuCalculationMode.StateCount,
          ihuDataType.String, tempValue, 1, interval, out DataRecords, out errors);
      Console.WriteLine("ihuRetrieveCalculatedDataEx2={0}", result);
      for (int i = 0; i < DataRecords.Length; i++)
      {
          using (IHU_RETRIEVED_DATA_VALUES_EX value = DataRecords[i])
          {
              Console.WriteLine("{0} [{1}] num={2}", value.Tagname, errors[i], value.NumberOfValues);

              IHU_TIMESTAMP[] times = value.TimeStamps;
              ihuValue[] values = value.DataValues;
              float[] percents = value.PercentGoods;
              IHU_COMMENT[][] comments = value.Comments;
              for (int j = 0; j < values.Length; j++)
              {
                  Console.WriteLine("  {0} value={1:0} good={2}%", times[j].ToDateTime(), values[j].AsObject(value.ValueDataType), percents[j]);
                  if (comments != null && comments[i] != null)
                      foreach (IHU_COMMENT comment in comments[i])
                          Console.WriteLine("    {0}", comment);
              }
          }
      }

      if (result == ihuErrorCode.OK)
      {
        string[] shortTags = new string[] { "test", "Shortx", "Short0010", "Short0100", "Short1000" };
        string[] intTags = new string[] { "Intx", "Int000000", "Int000001", "Int000002" };
        string[] floatTags = new string[] { "Float0011", "Float0099", "Float0999", "Float9999", "Floatx" };
        string[] doubleTags = new string[] { "Double0000", "Double0001", "Doublex", "Double0002" };
        string[] stringTags = new string[] { "String0000", "String0001", "Stringx" };
        string[] scaledTags = new string[] { "Scaled1000", "Scaledx", "Scaled9999" };

        string[] int64Tags = new string[] { "int64_001", "int64_002" };
        string[] uint64Tags = new string[] { "uint64_001", "uint64_002" };
        string[] uint32Tags = new string[] { "uint32_001", "uint32_001" };
        string[] uint16Tags = new string[] { "uint16_001", "uint16_001" };
        string[] byteTags = new string[] { "byte_001", "byte_001" };
        string[] boolTags = new string[] { "bool_001", "bool_001" };
        
        List<string> all = new List<string>();
        all.AddRange(shortTags);
        all.AddRange(intTags);
        all.AddRange(floatTags);
        all.AddRange(doubleTags);
        all.AddRange(stringTags);
        all.AddRange(scaledTags);
        all.AddRange(int64Tags);
        all.AddRange(uint64Tags);
        all.AddRange(uint32Tags);
        all.AddRange(uint16Tags);
        all.AddRange(byteTags);
        all.AddRange(boolTags);       

        string[] allTags = all.ToArray();

        CreateTags(handle, shortTags, ihuDataType.Short);
        CreateTags(handle, intTags, ihuDataType.Integer);
        CreateTags(handle, floatTags, ihuDataType.Float);
        CreateTags(handle, doubleTags, ihuDataType.DoubleFloat);
        CreateTags(handle, stringTags, ihuDataType.String);
        CreateTags(handle, scaledTags, ihuDataType.Scaled);
        CreateTags(handle, int64Tags, ihuDataType.Int64);
        CreateTags(handle, uint64Tags, ihuDataType.UInt64);
        CreateTags(handle, uint32Tags, ihuDataType.UInt32);
        CreateTags(handle, uint16Tags, ihuDataType.UInt16);
        CreateTags(handle, byteTags, ihuDataType.Byte);
        CreateTags(handle, boolTags, ihuDataType.Bool);

        TestPublishing(handle);
        CreateEnumeratedSets(handle);
        TestEnumeratedSets(handle);

        string[] props = new string[] { IHUAPI.ARCHIVER_PROP_CREATEOFFLINEARCHIVES, IHUAPI.ARCHIVER_PROP_CONFIGSERIALNUMBER, 
            IHUAPI.ARCHIVER_PROP_ACTIVEHOURS, IHUAPI.SECURITY_PROP_ALLOWANONYMOUSCLIENTS, IHUAPI.SECURITY_PROP_ANONYMOUSUSERNAME,
            IHUAPI.SECURITY_PROP_STRICTCLIENTAUTHENTICATION, IHUAPI.SECURITY_PROP_STRICTCOLLECTORAUTHENTICATION,
            IHUAPI.COLLECTOR_IDLE_TIME, "Foo" };

        int trials = 1;
        if (args.Length > 0)
          int.TryParse(args[0], out trials);

        for (int i = 0; i < trials; i++)
        {
         
          ReadRawDataByTime(handle, allTags);

          IHUAPI.ihuSetQueryModifiers(handle, "#ONLYGOOD");
          ReadRawDataByTime(handle, allTags);
          IHUAPI.ihuClearQueryModifiers();

          ReadRawDataByCount(handle, allTags, 10, true);
          ReadRawDataByCount(handle, allTags, 10, false);

          ReadMultiTagRawDataByTime(handle, allTags);
          ReadMultiTagRawDataByCount(handle, allTags, 10, true);
          ReadMultiTagRawDataByCount(handle, allTags, 10, false);

          RetrieveSampledData(handle, doubleTags);
          ReadInterpolatedValue(handle, doubleTags);
          ReadCurrentValue(handle, allTags);

          int val = (int)(DateTime.Now.Ticks % Int16.MaxValue);
          WriteData<short>(handle, shortTags, new short[] { (short)val, (short)(val - 1), (short)(val - 100), (short)(val >> 1), (short)(val >> 2) });
          WriteData<int>(handle, intTags, new int[] { val, val * 2, val * 10, val >> 1, val >> 2 });
          WriteData<float>(handle, floatTags, new float[] { val, val / 2, val / 10, val / 100, val * 10 });
          WriteData<double>(handle, doubleTags, new double[] { val, val / 2, val / 10, val / 100, val * 1000 });
          WriteData<string>(handle, stringTags, new string[] { String.Format("STR.{0}", val), String.Format("STR:{0}-{1}", val, val * 2), String.Format(">>>>> {0} <<<<<", val) } );
          WriteData<float>(handle, scaledTags, new float[] { val % 10000});

          WriteData<Int64>(handle, int64Tags, new Int64[] { 9223372036854775806, 9223372036854775807 });
          WriteData<UInt64>(handle, uint64Tags, new UInt64[] { 18446744073709551614, (UInt64)val % 10000 });
          WriteData<UInt32>(handle, uint32Tags, new UInt32[] { 4294967295, (UInt32)val % 10000 });
          WriteData<UInt16>(handle, uint16Tags, new UInt16[] { 65535, (UInt16)val });
          WriteData<Byte>(handle, byteTags, new Byte[] { 14, 11 });
          WriteData<SByte>(handle, boolTags, new SByte[] { 1, (SByte)val });

          GetArchiverProperty(handle, null, props);
          GetArchiverProperty(handle, "User", props);
          SetArchiverProperty(handle, null, props, new string[] { (i % 2).ToString(), DateTime.Now.Ticks.ToString(), (96 + i).ToString(), 
              DateTime.Now.Ticks.ToString(), "1","1","1","1",(96 + i).ToString()});

          SetArchiverProperty(handle, "User", props, new string[] { (i % 2).ToString(), DateTime.Now.Ticks.ToString(), (96 + i).ToString(), 
              DateTime.Now.Ticks.ToString(), (i % 2).ToString(),(i % 2).ToString(),(i % 2).ToString(),(i % 2).ToString(),(96 + i).ToString()});

          int numTags;
          result = IHUAPI.ihuFetchTagCache(handle, "*", out numTags);
          Console.WriteLine();
          Console.WriteLine("ihuFetchTagCache={0} [{1}]", numTags, result);

          GetTagPropertiesByTagname(allTags[i % allTags.Length]);
          GetTagPropertiesByIndex(i % numTags);

          result = IHUAPI.ihuCloseTagCache();
          Console.WriteLine();
          Console.WriteLine("ihuCloseTagCache [{0}]", result);

          TagAddRenameDelete(handle);

          Timestamps();

          BrowseCollectors(handle);

          BrowseQueryModifiers(handle);
        }

        Console.WriteLine();
        result = IHUAPI.ihuDisconnect(handle);
        Console.WriteLine("Disconnect={0}", result);
      }
    }
  }
}
