Proficy Historian UserAPI C# Wrapper
------------------------------------

ihuAPI.cs enables access to IHUAPI.dll from C# code. It abstracts marshaling memory between
managed C# application code and un-managed code in IHUAPI.dll. Public enums, structs and
entry points defined in IHUAPI.h are available as C# equivalents in ihuAPI.cs. Refer to IHUAPI.h
and the UserAPI online documentation for description of available functions. The exact IHUAPI.h
function and structure names have been retained in C# to accomodate the same set of online documentation.

The ihuAPI.cs requires Dot Net Framework 4.0 and Visual Studio 2010. 

To use in a C# project, simply add ihuAPI.cs to the project and add the following using directive:
using Proficy.Historian.UserAPI;

Alternatively, compile the UserAPI wrapper, ihuAPI.cs, with application code from the command line:
   csc.exe <MyApplication>.cs ihuAPI.cs

  
ihuAPI.cs abstracts both 32-bit and 64-bit IHUAPI.dll entry points but you have to match the appropriate
IHUAPI.dll to the OS or suffer a BadImageFormatException. Copy the respective IHUAPI.dll to the
application directory depending on your OS (32 or 64 bit).

It is possible to force an assembly to run as a 32-bit process on a 64-bit OS via the corflags.exe utility:
   corflags.exe [assembly] /32bit+
   corflags.exe [assembly] /32bit-


Refer to the included test.cs C# program for ihuAPI.cs usage and sample code.  The ReadData sample can als obe used to demonstrate raw and interpolated reads.
