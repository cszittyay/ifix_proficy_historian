﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Proficy.Historian.UserAPI;
using System.Runtime.InteropServices;

namespace ReportLike
{
	class Program
	{
		static void ReadRawDataByTime(int handle, string[] tags)
		{
			IHU_TIMESTAMP start = new IHU_TIMESTAMP(DateTime.Now.AddDays(-1));
			IHU_TIMESTAMP end = new IHU_TIMESTAMP(DateTime.Today);

			Console.WriteLine();
			Console.WriteLine("ihuReadRawDataByTime");
			foreach (string tag in tags)
			{
				IHU_DATA_SAMPLE[] values;
				ihuErrorCode result = IHUAPI.ihuReadRawDataByTime(handle, tag, start, end, out values);
				Console.WriteLine("{0} values={1} [{2}]", tag, values == null ? 0 : values.Length, result);
				for (int i = 0; i < (values == null ? 0 : values.Length); i++)
				{
					using (IHU_DATA_SAMPLE sample = values[i])
					{
						if (sample.ValueDataType != ihuDataType.Array)
							Console.WriteLine("  {0} {1} value={2:0}", sample.TimeStamp.ToDateTime(), sample.Quality, sample.ValueObject);
						else
						{
							DateTime time = sample.TimeStamp.ToDateTime();
							Console.WriteLine("  {0} {1} ", time.ToString("MM/dd/yy H:mm:ss:fff"), sample.Quality);
							ihuArrayValue aValue = (ihuArrayValue)Marshal.PtrToStructure(sample.Value.ArrayPtr, typeof(ihuArrayValue));
							Console.WriteLine("{0}", aValue.GetArrayValue.ToString());                           
						}
					}
				}
			}
		}
		static void RetrieveSampledData(int handle, string[] tags)
		{
			IHU_TIMESTAMP start = new IHU_TIMESTAMP();
			IHU_TIMESTAMP end = new IHU_TIMESTAMP(DateTime.Now);

			IHU_RETRIEVED_DATA_VALUES[] tagValues;
			ihuErrorCode[] errors;
			ihuErrorCode result = IHUAPI.ihuRetrieveSampledData(handle, tags, start, end, ihuSamplingMode.Interpolated, 24, 60 * 60 * 1000, out tagValues, out errors);

			Console.WriteLine();
			Console.WriteLine("ihuRetrieveSampledData={0}", result);
			for (int i = 0; i < tagValues.Length; i++)
			{
				using (IHU_RETRIEVED_DATA_VALUES value = tagValues[i])
				{
					Console.WriteLine("{0} [{1}] num={2}", value.Tagname, errors[i], value.NumberOfValues);

					IHU_TIMESTAMP[] times = value.TimeStamps;
					ihuValue[] values = value.DataValues;
					float[] percents = value.PercentGoods;
					IHU_COMMENT[][] comments = value.Comments;
					for (int j = 0; j < values.Length; j++)
					{
						Console.WriteLine("  {0} value={1:0} good={2}%", times[j].ToDateTime(), values[j].AsObject(value.ValueDataType), percents[j]);
						if (comments != null && comments[i] != null)
							foreach (IHU_COMMENT comment in comments[i])
								Console.WriteLine("    {0}", comment);
					}
				}
			}
		}
		static void ReadInterpolatedValue(int handle, string[] tags)
		{
			IHU_TIMESTAMP start = new IHU_TIMESTAMP(DateTime.Now.AddHours(-1));

			IHU_TIMESTAMP[] timestamps = { start, start };
			IHU_DATA_SAMPLE[] dataSamples;
			ihuErrorCode[] errors;
			ihuErrorCode result = IHUAPI.ihuReadInterpolatedValue(handle, tags, timestamps, out dataSamples, out errors);

			Console.WriteLine();
			Console.WriteLine("ihuReadInterpolatedValue={0}", result);
			for (int i = 0; i < dataSamples.Length; i++)
			{
				using (IHU_DATA_SAMPLE sample = dataSamples[i])
				{
					if (errors[i] == ihuErrorCode.OK)
						Console.WriteLine("{0} value={1:0} [{2}]", sample.Tagname, sample.ValueObject, errors[i]);
					else
						Console.WriteLine("{0} [{1}]", sample.Tagname, errors[i]);
				}
			}
		}

		static void Main(string[] args)
		{
			int handle;
			ihuErrorCode result;
			result = IHUAPI.ihuConnect("WS11", "szicom\\cszittyay", "CSZ.132410", out handle);
			Console.WriteLine();
			Console.WriteLine("Connect={0} handle={1}", result, handle);

			Console.WriteLine("ihuIsServerConnected({0})={1}", handle, IHUAPI.ihuIsServerConnected(handle));

			if (result == ihuErrorCode.OK)
			{
				// lenovo-B50.Simulation00001
				string[] floatTags = new string[] { "WS11.Simulation00001", "WS11.Simulation00002" };

				List<string> all = new List<string>();
				all.AddRange(floatTags);
				string[] allTags = all.ToArray();

				ReadRawDataByTime(handle, allTags);
				RetrieveSampledData(handle, floatTags);
				ReadInterpolatedValue(handle, floatTags);

				Console.WriteLine();
				result = IHUAPI.ihuDisconnect(handle);
				Console.WriteLine("Disconnect={0}", result);
			}
			Console.ReadKey();
		}
	}
}
